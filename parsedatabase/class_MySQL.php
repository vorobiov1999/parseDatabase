<?php
class MySQL{

	private $connect;

	private $tables = array();

	private $credentials = array();

	public function __construct($host, $user, $password, $database){
		$this->credentials["host"] = $host;
		$this->credentials["user"] = $user;
		$this->credentials["password"] = $password;
		$this->credentials["database"] = $database;

		$this->connect = new mysqli($host, $user, $password, $database);	
	}

	public function show_tables($refresh = false){
		if(empty($this->tables) || $refresh){
			$refreshTables = array();
			$result = $this->connect->query("SHOW TABLES");
			while($table = $result->fetch_assoc()['Tables_in_'.$this->credentials["database"]]){
				$refreshTables[] = $table;
			}
			$this->tables = $refreshTables;
		}
		return $this->tables;
	}

	public function delete_table($table){
		$result = $this->connect->query("DROP TABLE ".$table);
		return $result;
	}
}