<?php
	include "class_MySQL.php";
	include "class_Files.php";
	ini_set('error_reporting', null);
	ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	$mysql = new MySQL('localhost', 'root', '22792680', 'qwerty');
	$files = new Files();
	$fls = $files->all_files();
?>
	<div id="alltables" hidden><?php echo json_encode($mysql->show_tables());?></div>
	<div id="countfiles" hidden><?php echo count($fls);?></div>
<?php
	// foreach ($fls as $key => $value) {
	// 	foreach ($mysql->show_tables() as $key => $table) {
	// 		if($files->matches_in_file($value, $table)){
	// 			$uses[$table] = true;
	// 		}
	// 	}
	// }
?>
<!-- <style type="text/css">
	thead td{
		font-size: 30px;
		border: 0px solid black;
	}
	td{
		border: 1px solid black;
		padding: 10px;
		font-size: 25px;
	}
	thead tr{
		background-color: white;
	}
	tr{
		background-color: grey;
	}
	tbody tr:hover{
		background-color: white;
	}

</style> -->
<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>Unused tables</title>
   		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/index.js"></script>
   	</head>
   	<body>
		<div class="progress">
		  <div id="progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		    <span class="sr-only">45% Complete</span>
		  </div>
		</div>
   	<center>
   		<div class="page-header"><h1>Tables that are not used in your project</h1></div>
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							Table name
						</th>
						<th>
							<button type="button" class="btn btn-danger" id="all">Delete All</button>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						// $id=0;
						// foreach ($mysql->show_tables() as $key => $value) {
						// 	if(!isset($uses[$value])){
						// 		$id++;
						// 		echo "<tr id=\"tr".$id."\"><td>".$id."</td><td id=\"tablename\">".$value."</td>";
	?>
									<!-- <td>
										<button type="button" class="btn btn-danger" id=<?php echo "\"".$id."\"";?>>Delete</button>
									</td>
								</tr> -->
	<?php

						// 	}
						// }
						// //new_tabl
					?>
				</tbody>
			</table>
		</div>
	</body>
</html>