function draw(i) {
  $("#progress").attr("style", "width: "+i+"%");
}

$(document).ready(function(){
	var countfiles = $('#countfiles').text();
	var allFiles;
	var allTables = JSON.parse($('#alltables').text());
	var col = 0;
	var tables = [];
	
	function showUnusedTables(){
		for(var i = 0; i < allTables.length; i++){
			if(tables[allTables[i]] != true){
				$("tbody").append("<tr id=\"tr"+(i+1)+"\"></tr>");
				$("#tr"+(i+1)).append("<td>"+(i+1)+"</td><td id=\"tablename\">"+allTables[i]+"</td><td>"+
				"<button type=\"button\" class=\"btn btn-danger\" id=\""+(i+1)+"\">Delete</button>"+"</td>");		
			}
		}
		$("button").on("click", function(){
			var id = $(this).attr('id');		
			var tablename = "";
			var all = false;
			if(id == "all"){
				$("tbody tr").hide();
				all = true;
			}else{
				$("#tr"+id).hide();
				tablename = $("#tr"+id+" #tablename").text();
			}
			$.post("ajax/deleteTable.php",
			{
				"table": tablename,
				"all": all
			},
			function(data, status){
				console.log(data);
			});
		});
	}

	function matchesInFile(data){
		data = JSON.parse(data);
		col++;
		draw(100.0/(countfiles*allTables.length)*col);
		if(data.status){
			tables[data.table] = true;
		}
		if(col==countfiles*allTables.length){
			$(".progress").slideUp("slow");
			showUnusedTables();
		}		
	}

	function getAllFiles(data){
		allFiles = JSON.parse(data);
		for (var i = 0; i < countfiles; i++) {
			for(var j = 0; j < allTables.length; j++){
				$.ajax({
					url:"ajax/matchesInFile.php",
					method: "POST",
					data: {
						file: allFiles[i],
						table: allTables[j]
					},
					dataType: "html",
					success: matchesInFile
				});
			}
		}
	}

	$.ajax({
		url: "ajax/getAllFiles.php",
		dataType: "html",
		success: getAllFiles
	});
});