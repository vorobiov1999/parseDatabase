<?php
class Files{
	private $nowCatalog;
	private $files;
	private $patterns = array(
			"/.*\.php$/"
		);

	public function __construct(){
		$this->nowCatalog = dirname(__DIR__);
		$this->files = array();
	}

	private function get_files($dir){
		$entries = scandir($dir);
		$filelist = array();
		foreach ($entries as $key => $entry) {
			foreach ($this->patterns as $key1 => $pattern) {
			    if(preg_match($pattern, $entry)){
			    	$filelist[] = $dir."/".$entry;
			    }
			}
		}
		return $filelist;
	}

	private function get_catalogs($dir){
		$entries = scandir($dir);
		$dirlist = array();
		foreach ($entries as $key => $entry) {
			if(!empty(scandir($dir."/".$entry)) && $entry != '.' && $entry != '..'){
				$dirlist[] = $dir."/".$entry;
			}
		}
		return $dirlist;
	}

	public function all_files($refresh = false){
		if(empty($this->files) || $refresh){	
			$all_files = $this->get_files($this->nowCatalog);
			$catalogs = $this->get_catalogs($this->nowCatalog);
			$count_catalogs = 0;
			while(!empty($catalogs)){
				$nextCatalogs = array();
				foreach ($catalogs as $key => $dir) {
					$all_files = array_merge($all_files, $this->get_files($dir));
					$nextCatalogs = array_merge($nextCatalogs, $this->get_catalogs($dir));
				}
				$catalogs = $nextCatalogs;
				$count_catalogs += count($catalogs);
			}
			$this->files = $all_files;
		}
		return $this->files;
	}

	public function matches_in_file($file, $str){
		$content = file($file);
		foreach ($content as $key => $row) {
			if(preg_match("/.*".$str.".*/", $row)) {
				return true;
			}	
		}
		return false;
	}
}